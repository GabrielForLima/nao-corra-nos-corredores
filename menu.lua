-----------------------------------------------------------------------------------------
--
-- menu.lua
--
-----------------------------------------------------------------------------------------

local composer = require( "composer" )
local scene = composer.newScene()

-- include Corona's "widget" library
local widget = require "widget"

--------------------------------------------

-- forward declarations and other locals
local playBtn

-- 'onRelease' event listener for playBtn
local function onPlayBtnRelease()
	
	-- go to level1.lua scene
	audio.stop()
	composer.gotoScene( "level1", "fade", 500 )
	composer.removeScene( "menu" )
	
	return true	-- indicates successful touch
end

function scene:create( event )
	local sceneGroup = self.view
	local centerX = display.contentCenterX
	local centerY = display.contentCenterY
	local musicaintro = audio.loadSound ("ittybitty.mp3")

	-- Called when the scene's view does not exist.
	-- 
	-- INSERT code here to initialize the scene
	-- e.g. add display objects to 'sceneGroup', add touch listeners, etc.

	-- display a background image
	local background = display.newImageRect( "mapa2.png", display.actualContentWidth, display.actualContentHeight )
	background.anchorX = 0
	background.anchorY = 0
	background.x = 0 + display.screenOriginX 
	background.y = 0 + display.screenOriginY
	
	-- create/position logo/title image on upper-half of the screen
	local titleLogo = display.newText("Nao Corra", centerX, 100, "GameBoy.ttf", 18)
	local titleLogo2 = display.newText("nos Corredores!", centerX, 120, "GameBoy.ttf", 18)

	local intrucoes = display.newText("Regras: ", centerX, 150, "GameBoy.ttf", 18)
	
	local instrucoes2 = display.newText ("Voce esta atrasado para a Aula!", centerX, 220, "Gameboy.ttf", 8)
	instrucoes2:setFillColor( 0.5, 0, 0 )
	local instrucoes2 = display.newText (" Corra para chegar a tempo,", centerX, 235, "Gameboy.ttf", 8)
	instrucoes2:setFillColor( 0.5, 0, 0 )
	local instrucoes3 = display.newText ("desviando dos obstaculos e", centerX, 250, "Gameboy.ttf", 8)
	instrucoes3:setFillColor( 0.5, 0, 0 )
	local instrucoes4 = display.newText ("energizando-se com refrigerante!", centerX, 265, "Gameboy.ttf", 8)
	instrucoes4:setFillColor( 0.5, 0, 0 )

	
	
	-- create a widget button (which will loads level1.lua on release)
	playBtn = widget.newButton{
		label="Play Now",
		labelColor = { default={ 1, 1, 1 }, over={ 0, 0, 0, 0.5 } },
		default="button.png",
		font= "GameBoy.ttf",
		over="button-over.png",
		width=154, height=40,
		onRelease = onPlayBtnRelease	-- event listener function
	}
	playBtn:setFillColor( 0, 0, 0 )
	playBtn.x = display.contentCenterX
	playBtn.y = display.contentHeight - 125
	
	-- all display objects must be inserted into group
	sceneGroup:insert( background )
	sceneGroup:insert( titleLogo )
	sceneGroup:insert( playBtn )
	local intromusica = audio.play(musicaintro, {loops = -1})
end

function scene:show( event )
	local sceneGroup = self.view
	local phase = event.phase
	
	if phase == "will" then
		-- Called when the scene is still off screen and is about to move on screen
	elseif phase == "did" then
		-- Called when the scene is now on screen
		-- 
		-- INSERT code here to make the scene come alive
		-- e.g. start timers, begin animation, play audio, etc.
	end	
end

function scene:hide( event )
	local sceneGroup = self.view
	local phase = event.phase
	
	if event.phase == "will" then
		-- Called when the scene is on screen and is about to move off screen
		--
		-- INSERT code here to pause the scene
		-- e.g. stop timers, stop animation, unload sounds, etc.)
	elseif phase == "did" then
		-- Called when the scene is now off screen
	end	
end

function scene:destroy( event )
	local sceneGroup = self.view
	
	-- Called prior to the removal of scene's "view" (sceneGroup)
	-- 
	-- INSERT code here to cleanup the scene
	-- e.g. remove display objects, remove touch listeners, save state, etc.
	
	if playBtn then
		playBtn:removeSelf()	-- widgets must be manually removed
		playBtn = nil
	end
	audio.dispose (intromusica)
end

---------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

-----------------------------------------------------------------------------------------

return scene