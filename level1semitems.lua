-----------------------------------------------------------------------------------------
--
-- level1.lua
--
-----------------------------------------------------------------------------------------

local composer = require( "composer" )
local scene = composer.newScene()

-- include Corona's "physics" library
local physics = require "physics"

display.setStatusBar(display.HiddenStatusBar)

--------------------------------------------

-- forward declarations and other locals

math.randomseed( os.time() )




function scene:create( event )
	local screenW, screenH, halfW = display.actualContentWidth, display.actualContentHeight, display.contentCenterX
	local _W = display.contentWidth
	local _H = display.contentHeight
	local centerX = display.contentCenterX
	local centerY = display.contentCenterY
	local scrollspeed = 4
	local score = 0
	local vivo = true
	local backGroup = display.newGroup()  -- Display group for the background image
	local mainGroup = display.newGroup()  -- Display group for the ship, asteroids, lasers, etc.
	local uiGroup = display.newGroup()    -- Display group for UI objects like the score
	local sceneGroup = self.view
	local obstaculos = {}
	local itens = {}
	physics.start()
	physics.setGravity (0,0)
	local sheetOptions =
	{
    frames =
    	{
    		{
	    		--1- Fiscal
	    		x= 0,
	    		y= 0,
	    		width = 64,
	    		height = 64
    		},
 
    	},
	}
	local objectSheet = graphics.newImageSheet( "Fiscal.png", sheetOptions )

	local sheetOptions2 =
	{
	frames = 
		{

			{
			-- Elemento Neutro
			x = 0,
			y = 0,
			width = 32,
			height = 32



			},

		},

	}

	local objectSheet2 = graphics.newImageSheet ("NeutroSprite.png", sheetOptions2)


	local bg1 = display.newImageRect(backGroup,"background.jpg", 320, 480)
	bg1.x = _W*0.5;
	bg1.y = _H/2;	
	local bg2 = display.newImageRect(backGroup,"background.jpg", 320, 480)
	bg2.x = _W*0.5;
	bg2.y = bg1.y+480;
	local bg3 = display.newImageRect(backGroup,"background.jpg", 320, 480)
	bg3.x = _W*0.5;
	bg3.y = bg2.y+480;


	local player = display.newImageRect(mainGroup, "JogadorSprite.png", 32, 32)
	player.x = display.contentCenterX - 50
	player.y = _H
	physics.addBody( player ,"static", {radius = 10})
	player.myName= "player"

	scoreText = display.newText( uiGroup, "Evite os azuis, encoste nos cinzas: " .. score, centerX, centerY, native.systemFont, 16 )

	local function move(event)
		bg1.y = bg1.y + 8
		bg2.y = bg2.y + 8
		bg3.y = bg3.y + 8
		if (bg1.y + bg1.contentWidth) > 1040 then
			bg1:translate( 0, -960 )
		end
		if (bg2.y + bg2.contentWidth) > 1040 then
			bg2:translate( 0, -960 )
		end
		if (bg3.y + bg3.contentWidth) > 1040 then
			bg3:translate( 0, -960 )
		end
	end
	Runtime:addEventListener( "enterFrame", move )

	local function tapListener( event )
		
		if player.x == display.contentCenterX - 50 and player.y == _H then
			player.x = display.contentCenterX + 50
		else
			player.x = display.contentCenterX - 50
			player.y = _H
		end
	end
	player:addEventListener( "tap", tapListener )

	local function createObstacle(event)
		local newObstaculo = display.newImageRect(mainGroup, objectSheet, 1, 64, 64)
		table.insert (obstaculos, newObstaculo)
		physics.addBody( newObstaculo, "dynamic", { radius=10, bounce=0.8, isSensor = true } )
		newObstaculo.myName = "obstaculo"


		local whereFrom = math.random( 2 )
		 if ( whereFrom == 1 ) then
		 	newObstaculo.x =  centerX + 50
		 	newObstaculo.y =  1
		 	newObstaculo:setLinearVelocity( math.random( 0,0 ), math.random( 120,240 ) )
		 else
		 	newObstaculo.x =  centerX - 50
		 	newObstaculo.y = 1
		 	newObstaculo:setLinearVelocity( math.random( 0,0 ), math.random( 120,240 ) )
 
    	end

	end

	local function createItem(event)
		local newItem = display.newImageRect(mainGroup, objectSheet2, 1, 32, 32)
		table.insert (itens, newItem)
		physics.addBody( newItem, "dynamic", { radius=10, bounce=0.8, isSensor = true } )
		newItem.myName = "item"


		local whereFrom = math.random( 8 )
		 if ( whereFrom == 1 ) then
		 	newItem.x =  centerX + 50
		 	newItem.y =  1
		 	newItem:setLinearVelocity( math.random( 0,0 ), math.random( 60,120 ) )
		 elseif ( whereFrom == 2 ) then
		 	newItem.x =  centerX - 50
		 	newItem.y = 1
		 	newItem:setLinearVelocity( math.random( 0,0 ), math.random( 60,120 ) )
		 else
		 	thisItem = itens[i]
		 	display.remove (thisItem)
		 	table.remove (itens, i)
 
    	end

	end



	local function updateText()
	    scoreText.text = "Score: " .. score
	end

	local function addToScore()
	    score = score + 1
	end


	local function gameLoop()
		createObstacle()
		for i = #obstaculos, 1, -1 do
			thisObstaculo = obstaculos[i]
			if thisObstaculo.y > display.contentHeight + 100 then
				display.remove (thisObstaculo)
				table.remove (obstaculos, i)
				addToScore()
				timer.performWithDelay(500, updateText)
			end
    	end
    	createItem()
    	for i = #itens, 1, -1 do
			thisItem = itens[i]
			if thisItem.y > display.contentHeight + 100 then
				display.remove (thisItem)
				table.remove (itens, i)
			end
    	end
	end

	local function endGame()
		composer.gotoScene ("gameover", {time=800, effect="crossFade"})
		composer.removeScene('level1semitems')
	end

	local function onCollision( event )
	 
	    if ( event.phase == "began" ) then
	 
	        local obj1 = event.object1
	        local obj2 = event.object2

	        if ( ( obj1.myName == "player" and obj2.myName == "obstaculo" ) or
             ( obj1.myName == "obstaculo" and obj2.myName == "player" ) ) then
	        	display.remove (player)
	        	timer.performWithDelay (1000, endGame)
	        elseif ( ( obj1.myName == "player" and obj2.myName == "item" ) or
             ( obj1.myName == "item" and obj2.myName == "player" ) ) then
	        	display.remove (item)
	        	table.remove (itens, i)
	        	addToScore()
	        	updateText()
	        	
	        
        	end
	    end
	end
	Runtime:addEventListener( "collision", onCollision )

	gameLoopTimer = timer.performWithDelay( 2000, gameLoop, 0 )


	
	
end




function scene:show( event )
	local sceneGroup = self.view
	local phase = event.phase
	
	if phase == "will" then
		-- Called when the scene is still off screen and is about to move on screen
	elseif phase == "did" then
		-- Called when the scene is now on screen
		-- 
		-- INSERT code here to make the scene come alive
		-- e.g. start timers, begin animation, play audio, etc.
		physics.start()
	end
end

function scene:hide( event )
	local sceneGroup = self.view
	
	local phase = event.phase
	
	if event.phase == "will" then
		-- Called when the scene is on screen and is about to move off screen
		--
		-- INSERT code here to pause the scene
		-- e.g. stop timers, stop animation, unload sounds, etc.)

		timer.cancel( gameLoopTimer )

		physics.stop()
		Runtime:removeEventListener( "collision", onCollision )
        
        composer.removeScene( "level1" )

	elseif phase == "did" then
		-- Called when the scene is now off screen
		Runtime:removeEventListener( "collision", onCollision )
        
        composer.removeScene( "level1" )

	end	
	
end

function scene:destroy( event )

	-- Called prior to the removal of scene's "view" (sceneGroup)
	-- 
	-- INSERT code here to cleanup the scene
	-- e.g. remove display objects, remove touch listeners, save state, etc.
	local sceneGroup = self.view
	timer.cancel( gameLoopTimer )
	Runtime:removeEventListener( "collision", onCollision )
	Runtime:removeEventListener( "enterFrame", move )

	physics.stop()
	display.remove(backGroup)
	display.remove(mainGroup)
	display.remove(uiGroup)
    composer.removeScene( "level1" )
	package.loaded[physics] = nil
	physics = nil
	
end

---------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene.destroy )


-----------------------------------------------------------------------------------------

return scene