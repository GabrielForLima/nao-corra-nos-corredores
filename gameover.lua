local composer = require("composer")
local widget = require ("widget")


local scene = composer.newScene()
local playBtn
local titulo
local background
local song_end = audio.loadSound ("gameover.mp3")

function scene:create(event)
	background = display.newImageRect( "mapa2.png", display.actualContentWidth, display.actualContentHeight )
	background.anchorX = 0
	background.anchorY = 0
	background.x = 0 + display.screenOriginX 
	background.y = 0 + display.screenOriginY
	gameTitle = display.newText('Game Over', display.contentCenterX, display.contentCenterY, "GameBoy.ttf", 30)
    local gameovermusic = audio.play(song_end, {loops = 0})

	local function handleButtonEvent()
        audio.stop()
        composer.gotoScene("level1", {time= 400, effect = "fade"})
        composer.removeScene('gameover')
        

    
        return true -- indicates successful touch
    end

    local function handleButtonEventScore()
        audio.stop()
        composer.gotoScene("highscore", {time= 400, effect = "fade"})
        composer.removeScene('gameover')
    end

    playBtn = widget.newButton{
        label="Tentar De Novo?",
        labelColor = { default={255}, over={128} },
        default="button.png",
        font= "GameBoy.ttf",
        over="button-over.png",
        width=200, height=80,
        onRelease = handleButtonEvent    -- event listener function
    }
    playBtn.x = display.contentCenterX
    playBtn.y = display.contentHeight - 125


    playBtn2 = widget.newButton{
        label="Highscores",
        labelColor = { default={255}, over={128} },
        default="button.png",
        font= "GameBoy.ttf",
        over="button-over.png",
        width=200, height=80,
        onRelease = handleButtonEventScore    -- event listener function
    }
    playBtn2.x = display.contentCenterX
    playBtn2.y = display.contentHeight - 175


    
	
end

function scene:destroy(event)
    if playBtn then
        playBtn:removeSelf()    -- widgets must be manually removed
        playBtn = nil
    end
    if playBtn2 then
        playBtn2:removeSelf()    -- widgets must be manually removed
        playBtn2 = nil
    end
    display.remove(background)
    display.remove(titulo)
    display.remove(button1)
    audio.dispose (song_end)
end





scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )


-----------------------------------------------------------------------------------------

return scene