-----------------------------------------------------------------------------------------
--
-- level1.lua
--
-----------------------------------------------------------------------------------------

local composer = require( "composer" )
local scene = composer.newScene()
local spritesheets = require ("spritesheets")
local physics = require ("physics")

-- include Corona's "physics" library


display.setStatusBar(display.HiddenStatusBar)

--------------------------------------------

-- forward declarations and other locals

math.randomseed( os.time() )




function scene:create( event )
	physics.start()
	physics.pause()
	local itemPresente = false
	local screenW, screenH, halfW = display.actualContentWidth, display.actualContentHeight, display.contentCenterX
	local _W = display.contentWidth
	local _H = display.contentHeight
	local centerX = display.contentCenterX
	local centerY = display.contentCenterY
	local scrollspeed = 4
	local score = 0
	local velocidade = 120
	local musica = audio.loadSound("superman.mp3")
	local somLata = audio.loadSound("abrindoLata.mp3")
	local backGroup = display.newGroup()  -- Display group for the background image
	local mainGroup = display.newGroup()  -- Display group for the ship, asteroids, lasers, etc.
	local uiGroup = display.newGroup()    -- Display group for UI objects like the score
	local sceneGroup = self.view
	local obstaculos = {}
	local efeito = audio.loadSound("morte.mp3")
	local itens = {}
	local velocidadeloop = 800
	physics.start()
	physics.setGravity (0,0)

	-- As sheets de Tudo-------------------------------------------------
	local sheet_item = {
		width = 64,
		height = 64, 
		numFrames = 3

	}

	local item_animation = {
		{
			name = 'item_fizzling',
			start = 1, 
			count = 3,
			time = 800,
			loopcount = 0,
			loopDirection = "forward"


		}



	}




	local sheet_jogador = {
		width = 64,
		height = 64,
		numFrames = 2

	}

	local jogador_animation = {
		{
			name= 'jogadorrun',
			start = 1,
			count = 2,
			time = 800, 
			loopcount = 0,
			loopDirection = "forward"





		}
		



	}

	local jogadorsheet = graphics.newImageSheet ("Sprites/Jogadorclone.png", sheet_jogador)
	local boisheet = graphics.newImageSheet ("Sprites/ObstaculoBoi.png", sheet_jogador)
	local itemsheet = graphics.newImageSheet ("Sprites/colateste.png", sheet_item)

	local obstaculo_sheet = {
		width = 64,
		height = 64,
		numFrames = 3


	}

	local obstaculo_animation = {

		name = 'pocaanimation',
		start= 1,
		count = 3,
		time = 1600,
		loopcount = 0,
		loopDirection = "forward"

	}

	local obstaculosheet = graphics.newImageSheet ("Sprites/Poca.png", obstaculo_sheet)


	



	------------------------------------------------------------ Linha Divisoria ----------------------------------------------------------------

	


	local bg1 = display.newImageRect(backGroup,"mapa2.png", 320, 600)
	bg1.x = _W*0.5;
	bg1.y = _H/2;	
	local bg2 = display.newImageRect(backGroup,"mapa2.png", 320, 600)
	bg2.x = _W*0.5;
	bg2.y = bg1.y+480;
	local bg3 = display.newImageRect(backGroup,"mapa2.png", 320, 600)
	bg3.x = _W*0.5;
	bg3.y = bg2.y+480;


	local player = display.newSprite(mainGroup,jogadorsheet, jogador_animation)
	player.x = display.contentCenterX - 50
	player.y = _H
	physics.addBody( player ,"static", {radius = 15})
	player.myName= "player"
	player:play()

	scoreText = display.newText( uiGroup, "Corra, voce esta atrasado!: " .. score, centerX, centerY, "GameBoy.ttf", 12)

	local function move(event)
		bg1.y = bg1.y + scrollspeed
		bg2.y = bg2.y + scrollspeed
		bg3.y = bg3.y + scrollspeed
		if (bg1.y + bg1.contentWidth) > 1040 then
			bg1:translate( 0, -960 )
		end
		if (bg2.y + bg2.contentWidth) > 1040 then
			bg2:translate( 0, -960 )
		end
		if (bg3.y + bg3.contentWidth) > 1040 then
			bg3:translate( 0, -960 )
		end
	end
	Runtime:addEventListener( "enterFrame", move )

	local function tapListener( event )
		
		if player.x == display.contentCenterX - 50 and player.y == _H then
			player.x = display.contentCenterX + 50
		else
			player.x = display.contentCenterX - 50
			player.y = _H
		end
	end
	player:addEventListener( "tap", tapListener )

	local function createObstacle(event)
		local newObstaculo = display.newSprite(mainGroup,obstaculosheet, obstaculo_animation)
		table.insert (obstaculos, newObstaculo)
		physics.addBody( newObstaculo, "dynamic", { radius=10, bounce=0.8, isSensor = true } )
		newObstaculo.myName = "obstaculo"
		newObstaculo:play()


		local whereFrom = math.random( 2 )
		 if ( whereFrom == 1 ) then
		 	newObstaculo.x =  centerX + 50
		 	newObstaculo.y =  1
		 	newObstaculo:setLinearVelocity( math.random( 0,0 ), math.random( velocidade,velocidade ) )
		 else
		 	newObstaculo.x =  centerX - 50
		 	newObstaculo.y = 1
		 	newObstaculo:setLinearVelocity( math.random( 0,0 ), math.random( velocidade,velocidade ) )
 
    	end

	end

	local function aumentarVelocidade()
		velocidade = velocidade + velocidade*0.02
		scrollspeed = scrollspeed + scrollspeed*0.02
		velocidadeloop = velocidadeloop - velocidadeloop*0.80

	end

	



	-- Para Som ( Fora do Loop, para que ele não repita )
	local options = {
		loops = -1,
	}
	local jogomusica = audio.play(musica, {loops = -1})

	

	local function createItem(event)
		local newItem = display.newSprite(mainGroup,itemsheet, item_animation)
		table.insert (itens, newItem)
		physics.addBody( newItem, "dynamic", { radius=10, bounce=0.8, isSensor = true } )
		newItem.myName = "item"
		


		local whereFrom = math.random( 2 )
		 if ( whereFrom == 1 ) then
		 	newItem.x =  centerX + 50
		 	newItem.y =  1
		 	newItem:setLinearVelocity( math.random( 0,0 ), math.random( velocidade,velocidade ) )
		 elseif ( whereFrom == 2 ) then
		 	newItem.x =  centerX - 50
		 	newItem.y = 1
		 	newItem:setLinearVelocity( math.random( 0,0 ), math.random( velocidade,velocidade ) )
		 
    	end

	end

	local function ObjectOrItem()
		local which = math.random (8)
			if (which == 1) and (itemPresente == false) then
				createItem()
				itemPresente = true
			else
				createObstacle()
		end
	end




	local function updateText()
	    scoreText.text = "Score: " .. score
	end

	local function addToScore()
	    score = score + 1
	end


	local function gameLoop()

		ObjectOrItem()
		timer.performWithDelay(10000, aumentarVelocidade )
		for i = #obstaculos, 1, -1 do
			thisObstaculo = obstaculos[i]
			if thisObstaculo.y > display.contentHeight + 100 then
				display.remove (thisObstaculo)
				table.remove (obstaculos, i)
				addToScore()
				updateText()
			end
    	end
    	for i = #itens, 1, -1 do
			thisItem = itens[i]
			if thisItem.y > display.contentHeight + 100 then
				display.remove (thisItem)
				table.remove (itens, i)
				itemPresente = false
			end
    	end
	end


		--[[createObstacle()
		for i = #obstaculos, 1, -1 do
			thisObstaculo = obstaculos[i]
			if thisObstaculo.y > display.contentHeight + 100 then
				display.remove (thisObstaculo)
				table.remove (obstaculos, i)
				addToScore()
				timer.performWithDelay(500, updateText)
			end
    	end
    	createItem()
    	for i = #itens, 1, -1 do
			thisItem = itens[i]
			if thisItem.y > display.contentHeight + 100 then
				display.remove (thisItem)
				table.remove (itens, i)
			end
    	end
	end]]

	local function endGame()
		composer.setVariable( "finalScore", score )
		composer.gotoScene ("gameover", {time=800, effect="crossFade"})
		composer.removeScene('level1')

	end

	local function onCollision( event )
	 
	    if ( event.phase == "began" ) then
	 
	        local obj1 = event.object1
	        local obj2 = event.object2

	        if ( ( obj1.myName == "player" and obj2.myName == "obstaculo" ) or
             ( obj1.myName == "obstaculo" and obj2.myName == "player" ) ) then
	        	local impacto = audio.play(efeito, {loops = 0})
	        	display.remove (player)
	        	timer.performWithDelay (1500, endGame)
	        	audio.stop (jogomusica)
	        elseif ( ( obj1.myName == "player" and obj2.myName == "item" ) or
             ( obj1.myName == "item" and obj2.myName == "player" ) ) then
	        	for i = #itens, 1, -1 do
	        		local lataGet = audio.play(somLata, {loops = 0})
					thisItem = itens[i]
					display.remove (thisItem)
					table.remove (itens, i)
					addToScore()
					updateText()
					itemPresente = false
				end    
        	end
	    end
	end
	Runtime:addEventListener( "collision", onCollision )

	gameLoopTimer = timer.performWithDelay( velocidadeloop, gameLoop, 0 )


	
	
end




function scene:show( event )
	local sceneGroup = self.view
	local phase = event.phase
	
	if phase == "will" then
		-- Called when the scene is still off screen and is about to move on screen
	elseif phase == "did" then
		-- Called when the scene is now on screen
		-- 
		-- INSERT code here to make the scene come alive
		-- e.g. start timers, begin animation, play audio, etc.
		physics.start()
	end
end

function scene:hide( event )
	local sceneGroup = self.view
	
	local phase = event.phase
	
	if event.phase == "will" then
		-- Called when the scene is on screen and is about to move off screen
		--
		-- INSERT code here to pause the scene
		-- e.g. stop timers, stop animation, unload sounds, etc.)

		timer.cancel( gameLoopTimer )

		physics.stop()
		Runtime:removeEventListener( "collision", onCollision )
        
        

	elseif phase == "did" then
		-- Called when the scene is now off screen
		Runtime:removeEventListener( "collision", onCollision )
		composer.removeScene( "level1" )
        
        

	end	
	
end

function scene:destroy( event )

	-- Called prior to the removal of scene's "view" (sceneGroup)
	-- 
	-- INSERT code here to cleanup the scene
	-- e.g. remove display objects, remove touch listeners, save state, etc.
	local sceneGroup = self.view
	timer.cancel(gameLoopTimer)
	Runtime:removeEventListener( "tap", tapListener )
	Runtime:removeEventListener( "collision", onCollision )
	Runtime:removeEventListener( "enterFrame", move )

	physics.stop()
	--package.loaded[physics] = nil
	--physics = nil
	display.remove(backGroup)
	display.remove(mainGroup)
	display.remove(uiGroup)
    --composer.removeScene( "level1" )
	audio.dispose(somLata)
	audio.dispose(jogomusica)
	audio.dispose(impacto)
	
end

---------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene)


-----------------------------------------------------------------------------------------

return scene